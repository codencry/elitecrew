import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { controllers } from 'src/comment/application/controller';
import { DomainModule } from 'src/comment/domain/domain.module';

@Module({
  imports: [DomainModule, CqrsModule],
  controllers: [...controllers],
})
export class ApplicationModule {}
