import { IsNotEmpty } from 'class-validator';

export class IdQuery {
  @IsNotEmpty()
  id: string;
}
