import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiResponse } from '@nestjs/swagger';
import { IdQuery } from 'src/comment/application/controller/query/id.query';
import { CommentRequestDto } from 'src/comment/application/controller/request/comment-request.dto';
import { EditCommentRequestDto } from 'src/comment/application/controller/request/edit-comment-request.dto';
import { SubCommentRequestDto } from 'src/comment/application/controller/request/sub-comment-request.dto';
import { AddSubCommentCommand } from 'src/comment/domain/core/command/impl/add-subcomment.command';
import { CreateCommentCommand } from 'src/comment/domain/core/command/impl/create-comment.command';
import { DeleteCommentCommand } from 'src/comment/domain/core/command/impl/delete-comment.command';
import { EditCommentCommand } from 'src/comment/domain/core/command/impl/edit-comment.command';
import { CommentDto } from 'src/comment/domain/core/comment.dto';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { ListCommentsQuery } from 'src/comment/domain/core/query/impl/list-comments.query';

@Controller('comment')
export class CommentController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @ApiResponse({
    type: CommentDto,
  })
  @Post()
  async create(
    @Body() commentRequestDto: CommentRequestDto,
  ): Promise<CommentDto> {
    return (
      await (<Promise<CommentEntity>>(
        this.commandBus.execute(
          new CreateCommentCommand(
            commentRequestDto.content,
            commentRequestDto.postId,
          ),
        )
      ))
    ).toDto();
  }

  @ApiResponse({
    type: CommentDto,
  })
  @Post('sub-comment')
  async addSubComment(
    @Body() subCommentRequestDto: SubCommentRequestDto,
  ): Promise<CommentDto> {
    return (
      await (<Promise<CommentEntity>>(
        this.commandBus.execute(
          new AddSubCommentCommand(
            subCommentRequestDto.content,
            subCommentRequestDto.parentId,
          ),
        )
      ))
    ).toDto();
  }

  @ApiResponse({
    type: [CommentDto],
  })
  @Get()
  async list(): Promise<CommentDto[]> {
    return (
      await (<Promise<CommentEntity[]>>(
        this.queryBus.execute(new ListCommentsQuery())
      ))
    ).map((comment) => comment.toDto());
  }

  @ApiResponse({})
  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param() idQuery: IdQuery): Promise<void> {
    await this.commandBus.execute(new DeleteCommentCommand(idQuery.id));
  }

  @ApiResponse({
    type: CommentDto,
  })
  @Patch(':id')
  async update(
    @Param() idQuery: IdQuery,
    @Body() editCommentRequestDto: EditCommentRequestDto,
  ): Promise<CommentDto> {
    return (
      await (<Promise<CommentEntity>>(
        this.commandBus.execute(
          new EditCommentCommand(idQuery.id, editCommentRequestDto.content),
        )
      ))
    ).toDto();
  }
}
