import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class EditCommentRequestDto {
  @ApiProperty()
  @IsNotEmpty()
  content: string;
}
