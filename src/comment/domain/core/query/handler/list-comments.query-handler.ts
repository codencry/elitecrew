import { Inject } from '@nestjs/common';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { ListCommentsQuery } from 'src/comment/domain/core/query/impl/list-comments.query';
import { ListCommentsUseCase } from 'src/comment/domain/port/incoming/list-comments.usecase';
import {
  CommentEntityRepository,
  COMMENT_ENTITY_REPOSITORY_TOKEN,
} from 'src/comment/domain/port/outgoing/comment-entity-repository.port';

@QueryHandler(ListCommentsQuery)
export class ListCommentsQueryHandler
  implements IQueryHandler<ListCommentsQuery>, ListCommentsUseCase
{
  @Inject(COMMENT_ENTITY_REPOSITORY_TOKEN)
  private readonly commentEntityRepository: CommentEntityRepository;

  async execute(query: ListCommentsQuery): Promise<CommentEntity[]> {
    return await this.commentEntityRepository.findAll();
  }
}
