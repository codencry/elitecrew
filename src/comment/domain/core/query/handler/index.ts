import { ListCommentsQueryHandler } from 'src/comment/domain/core/query/handler/list-comments.query-handler';

export const queryHandlers = [ListCommentsQueryHandler];
