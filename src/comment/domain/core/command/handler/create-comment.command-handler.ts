import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { CreateCommentCommand } from 'src/comment/domain/core/command/impl/create-comment.command';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { CreateCommentUseCase } from 'src/comment/domain/port/incoming/create-comment.usercase';
import {
  CommentEntityRepository,
  COMMENT_ENTITY_REPOSITORY_TOKEN,
} from 'src/comment/domain/port/outgoing/comment-entity-repository.port';

@CommandHandler(CreateCommentCommand)
export class CreateCommentCommandHandler
  implements ICommandHandler<CreateCommentCommand>, CreateCommentUseCase
{
  @Inject(COMMENT_ENTITY_REPOSITORY_TOKEN)
  private readonly commentEntityRepository: CommentEntityRepository;

  async execute(command: CreateCommentCommand): Promise<CommentEntity> {
    return await this.commentEntityRepository.save(
      CommentEntity.create({
        ...command,
      }),
    );
  }
}
