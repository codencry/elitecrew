import { AddSubCommentCommandHandler } from 'src/comment/domain/core/command/handler/add-subcomment.command-handler';
import { CreateCommentCommandHandler } from 'src/comment/domain/core/command/handler/create-comment.command-handler';
import { DeleteCommentCommandHandler } from 'src/comment/domain/core/command/handler/delete-comment.command-handler';
import { EditCommentCommandHandler } from 'src/comment/domain/core/command/handler/edit-comment.command-handler';

export const commandHandlers = [
  CreateCommentCommandHandler,
  DeleteCommentCommandHandler,
  AddSubCommentCommandHandler,
  EditCommentCommandHandler,
];
