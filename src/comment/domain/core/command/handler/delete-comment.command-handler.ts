import { Inject, NotFoundException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { DeleteCommentCommand } from 'src/comment/domain/core/command/impl/delete-comment.command';
import { DeleteCommentUseCase } from 'src/comment/domain/port/incoming/delete-comment.usecase';
import {
  CommentEntityRepository,
  COMMENT_ENTITY_REPOSITORY_TOKEN,
} from 'src/comment/domain/port/outgoing/comment-entity-repository.port';

@CommandHandler(DeleteCommentCommand)
export class DeleteCommentCommandHandler
  implements ICommandHandler<DeleteCommentCommand>, DeleteCommentUseCase
{
  @Inject(COMMENT_ENTITY_REPOSITORY_TOKEN)
  private readonly commentEntityRepository: CommentEntityRepository;

  async execute(command: DeleteCommentCommand): Promise<void> {
    const commentEntity = await this.commentEntityRepository.findById(
      command.commentId,
    );

    if (!commentEntity) {
      throw new NotFoundException();
    }

    await this.commentEntityRepository.delete(commentEntity);
  }
}
