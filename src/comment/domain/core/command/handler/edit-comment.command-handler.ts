import { Inject, NotFoundException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { EditCommentCommand } from 'src/comment/domain/core/command/impl/edit-comment.command';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { EditCommentUseCase } from 'src/comment/domain/port/incoming/edit-comment.usecase';
import {
  CommentEntityRepository,
  COMMENT_ENTITY_REPOSITORY_TOKEN,
} from 'src/comment/domain/port/outgoing/comment-entity-repository.port';

@CommandHandler(EditCommentCommand)
export class EditCommentCommandHandler
  implements ICommandHandler<EditCommentCommand>, EditCommentUseCase
{
  @Inject(COMMENT_ENTITY_REPOSITORY_TOKEN)
  private readonly commentEntityRepository: CommentEntityRepository;

  async execute(command: EditCommentCommand): Promise<CommentEntity> {
    const { content, id } = command;
    const commentEntity = await this.commentEntityRepository.findById(id);

    if (!commentEntity) {
      throw new NotFoundException();
    }

    commentEntity.content = content;

    return await this.commentEntityRepository.save(commentEntity);
  }
}
