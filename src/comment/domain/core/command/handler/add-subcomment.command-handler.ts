import { BadRequestException, Inject, NotFoundException } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AddSubCommentCommand } from 'src/comment/domain/core/command/impl/add-subcomment.command';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { AddSubCommentUseCase } from 'src/comment/domain/port/incoming/add-subcomment.usecase';
import {
  CommentEntityRepository,
  COMMENT_ENTITY_REPOSITORY_TOKEN,
} from 'src/comment/domain/port/outgoing/comment-entity-repository.port';

@CommandHandler(AddSubCommentCommand)
export class AddSubCommentCommandHandler
  implements ICommandHandler<AddSubCommentCommand>, AddSubCommentUseCase
{
  @Inject(COMMENT_ENTITY_REPOSITORY_TOKEN)
  private readonly commentEntityRepository: CommentEntityRepository;

  async execute(command: AddSubCommentCommand): Promise<CommentEntity> {
    const { content, parentId } = command;
    const parentCommentEntity = await this.commentEntityRepository.findById(
      parentId,
    );

    if (!parentCommentEntity) {
      throw new NotFoundException();
    }

    if (parentCommentEntity.parent) {
      throw new BadRequestException(
        'Only one level of nesting comments is allowed.',
      );
    }

    return await this.commentEntityRepository.save(
      parentCommentEntity.addSubComment({
        content: content,
      }),
    );
  }
}
