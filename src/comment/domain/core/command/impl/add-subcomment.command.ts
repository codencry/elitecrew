export class AddSubCommentCommand {
  constructor(
    public readonly content: string,
    public readonly parentId: string,
  ) {}
}
