import { ApiProperty } from '@nestjs/swagger';

export class CommentDto {
  @ApiProperty()
  id: string;

  @ApiProperty()
  content: string;

  @ApiProperty()
  parent: string;

  @ApiProperty({ type: CommentDto })
  subComments: CommentDto[];

  @ApiProperty()
  postId: string;
}
