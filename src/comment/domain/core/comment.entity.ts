import { CommentDto } from 'src/comment/domain/core/comment.dto';
import {
  Column,
  Entity,
  JoinTable,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { v4 as uuid } from 'uuid';

export class CreateCommenEntityPayload {
  content: string;
  postId: string;
  parent?: CommentEntity;
}

export interface AddSubCommentPayload {
  content: string;
}

@Entity()
@Tree('materialized-path')
export class CommentEntity {
  @PrimaryColumn({
    type: 'uuid',
  })
  id: string;

  @Column({
    type: 'text',
  })
  content: string;

  @TreeParent({
    onDelete: 'CASCADE',
  })
  parent?: CommentEntity;

  @TreeChildren()
  subComments: CommentEntity[];

  @Column({
    type: 'uuid',
  })
  postId: string;

  toDto(): CommentDto {
    return Object.assign(new CommentDto(), this);
  }

  public static create(payload: CreateCommenEntityPayload): CommentEntity {
    return Object.assign(new CommentEntity(), {
      ...payload,
      id: uuid(),
    });
  }

  addSubComment(payload: AddSubCommentPayload): CommentEntity {
    return CommentEntity.create({
      content: payload.content,
      postId: this.postId,
      parent: this,
    });
  }
}
