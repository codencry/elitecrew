import { CommentEntity } from 'src/comment/domain/core/comment.entity';

export const COMMENT_ENTITY_REPOSITORY_TOKEN =
  'COMMENT_ENTITY_REPOSITORY_TOKEN';

export interface CommentEntityRepository {
  save(entity: CommentEntity): Promise<CommentEntity>;
  findById(id: string): Promise<CommentEntity>;
  delete(entity: CommentEntity): Promise<void>;
  findAll(): Promise<CommentEntity[]>;
}
