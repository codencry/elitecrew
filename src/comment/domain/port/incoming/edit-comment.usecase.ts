import { EditCommentCommand } from 'src/comment/domain/core/command/impl/edit-comment.command';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';

export interface EditCommentUseCase {
  execute(command: EditCommentCommand): Promise<CommentEntity>;
}
