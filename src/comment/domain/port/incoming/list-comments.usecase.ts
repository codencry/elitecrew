import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { ListCommentsQuery } from 'src/comment/domain/core/query/impl/list-comments.query';

export interface ListCommentsUseCase {
  execute(query: ListCommentsQuery): Promise<CommentEntity[]>;
}
