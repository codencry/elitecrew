import { DeleteCommentCommand } from 'src/comment/domain/core/command/impl/delete-comment.command';

export interface DeleteCommentUseCase {
  execute(command: DeleteCommentCommand): Promise<void>;
}
