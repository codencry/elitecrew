import { AddSubCommentCommand } from 'src/comment/domain/core/command/impl/add-subcomment.command';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';

export interface AddSubCommentUseCase {
  execute(comment: AddSubCommentCommand): Promise<CommentEntity>;
}
