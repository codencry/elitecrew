import { CreateCommentCommand } from 'src/comment/domain/core/command/impl/create-comment.command';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';

export interface CreateCommentUseCase {
  execute(command: CreateCommentCommand): Promise<CommentEntity>;
}
