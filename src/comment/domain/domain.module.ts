import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { commandHandlers } from 'src/comment/domain/core/command/handler';
import { queryHandlers } from 'src/comment/domain/core/query/handler';
import { InfrastructureModule } from 'src/comment/infrastructure/infrastructure.module';

@Module({
  imports: [InfrastructureModule, CqrsModule],
  providers: [...commandHandlers, ...queryHandlers],
})
export class DomainModule {}
