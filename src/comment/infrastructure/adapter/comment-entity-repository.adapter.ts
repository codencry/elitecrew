import { Injectable } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { CommentEntityRepository } from 'src/comment/domain/port/outgoing/comment-entity-repository.port';
import { EntityManager } from 'typeorm';

@Injectable()
export class CommentEntityRepositoryAdapter implements CommentEntityRepository {
  constructor(
    @InjectEntityManager()
    private readonly entityManager: EntityManager,
  ) {}

  async findAll(): Promise<CommentEntity[]> {
    return await this.entityManager
      .getTreeRepository(CommentEntity)
      .findTrees();
  }

  async findById(id: string): Promise<CommentEntity> {
    return await this.entityManager.getRepository(CommentEntity).findOne(id, {
      relations: ['parent'],
    });
  }

  async delete(entity: CommentEntity): Promise<void> {
    await this.entityManager.getRepository(CommentEntity).delete(entity);
  }

  async save(entity: CommentEntity): Promise<CommentEntity> {
    return await this.entityManager.getRepository(CommentEntity).save(entity);
  }
}
