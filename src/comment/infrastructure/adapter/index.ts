import { Provider } from '@nestjs/common';
import { COMMENT_ENTITY_REPOSITORY_TOKEN } from 'src/comment/domain/port/outgoing/comment-entity-repository.port';
import { CommentEntityRepositoryAdapter } from 'src/comment/infrastructure/adapter/comment-entity-repository.adapter';

export const adapters: Provider[] = [
  {
    provide: COMMENT_ENTITY_REPOSITORY_TOKEN,
    useClass: CommentEntityRepositoryAdapter,
  },
];
