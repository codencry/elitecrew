import { MigrationInterface, QueryRunner } from 'typeorm';

export class init1630518000746 implements MigrationInterface {
  name = 'init1630518000746';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "comment_entity" ("id" uuid NOT NULL, "content" text NOT NULL, "postId" uuid NOT NULL, "mpath" character varying DEFAULT '', "parentId" uuid, CONSTRAINT "PK_5a439a16c76d63e046765cdb84f" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `ALTER TABLE "comment_entity" ADD CONSTRAINT "FK_94d540d1210eb47d8c42048365e" FOREIGN KEY ("parentId") REFERENCES "comment_entity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "comment_entity" DROP CONSTRAINT "FK_94d540d1210eb47d8c42048365e"`,
    );
    await queryRunner.query(`DROP TABLE "comment_entity"`);
  }
}
