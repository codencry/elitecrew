import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const typeormConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: false,
  entities: ['dist/comment/domain/core/**/*.entity.js'],
  migrations: ['dist/comment/infrastructure/migrations/*.js'],
  cli: {
    migrationsDir: 'src/comment/infrastructure/migrations',
  },
  migrationsTableName: 'migration',
  autoLoadEntities: true,
};

export = typeormConfig;
