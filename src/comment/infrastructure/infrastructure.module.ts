import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentEntity } from 'src/comment/domain/core/comment.entity';
import { adapters } from 'src/comment/infrastructure/adapter';

@Module({
  imports: [TypeOrmModule.forFeature([CommentEntity])],
  providers: [...adapters],
  exports: [...adapters],
})
export class InfrastructureModule {}
